var socket = io();
socket.on("response", function (data) {
    data = JSON.parse(data);
    let mensaje = "<b>Resultado:</b> <br>";
    let variables = parseInt(document.getElementById('nd').value);
	for(let k=1; k<=variables; k++)
        mensaje += "x"+ k + " = " + data['result']['x'+k] + "<br>";
    mensaje += "Z = " + data['result']['z']; 
    
    $('#respuesta').html(mensaje)
})

function reescribir() {
    //
    let tipo = document.getElementById('objetivo').value;
    tipo += "imize";
    // Parsear FO
    var variables = parseInt(document.getElementById('nd').value);
    var restricciones = parseInt(document.getElementById('nr').value);

    var feo = "";
    for (let j = 1; j <= variables; j++) {
        if ($('#X' + j).val() > 0) {
            feo += "+" + $('#X' + j).val() + "x" + j + " ";
        } else {
            feo += $('#X' + j).val() + "x" + j + " ";
        }
    }

    //FIN PARSEAR FO

    // PARSEAR RESTRICCIONES
    let ress = "";
    let vector = [];
    for (i = 1; i <= restricciones; i++) {
        for (j = 1; j <= variables; j++) {
            let constante = document.getElementById(i + 'X' + j).value;
            if (j > 1 && constante >= 0) ress += "+";
            ress += constante + "x" + j;    
                  
        }
        if( $("#btnSig" + i).val() == '≥') ress += ">=";
        if( $("#btnSig" + i).val() == '≤') ress += "<=";
        if( $("#btnSig" + i).val() == '=') ress += "=";
        
        ress += $('#R'+i).val();
        vector.push(ress);
        ress = "";
    }
    var input = {
        type: tipo,
        objective : feo,
        constraints : vector
    };
    socket.emit('start', input);
}

function checkMetodoFase2(){
    let tipo = document.getElementById('objetivo').value;
    let restricciones = parseInt(document.getElementById('nr').value);
    let igualdad;
    if(tipo == "max") igualdad = "≥";
    else if(tipo == "min") igualdad = "≤";
    
    for (i = 1; i <= restricciones; i++) {
        if( $("#btnSig" + i).val() == igualdad ) 
            return true;
    }
    return false;    
}

function EscojeCamino(){
    if( checkMetodoFase2() ){
        // Hacer lo de Nodejs
        reescribir();
    }else{
        iterar();
    }
}