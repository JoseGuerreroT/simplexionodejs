cont = 0;
var checkHolguras = 0;

function crea_matriz(filas, columnas) {
	matriz = new Array(filas);
	for (i = 0; i < filas; i++) {
		matriz[i] = new Array(columnas)
		for (j = 0; j < columnas; j++) {
			matriz[i][j] = 0;
		}
	}
}

function generaMatrizInputs() {
	var columnas = parseInt(document.getElementById('nd').value);
	var filas = parseInt(document.getElementById('nr').value);
	let valueObjetivo = $('#objetivo').val();
	if (!valueObjetivo || !columnas || !filas) {
		if (!valueObjetivo) alert("Selecciona objetivo: Maximizar/Minimizar.");
		else if (!filas) alert("Digite Numero de variables de desición.");
		else if (!columnas) alert("Digite Numero de variables de desición");
	} else {
		var fo = '';
		var aux = '';
		var matriz = '';
		var ceros = '';
		fo += '<div class="row">';
		for (i = 1; i <= columnas; i++) {

			fo += '<input type="number" value="0" name="' + 'X' + i + '" id="' + 'X' + i + '" onClick="this.select();" required="required" style="width: 10%; text-align:right;" /><b> X' + i + '</b>';
			ceros += 'X' + i;
			if (i != columnas) {
				fo += ' + ';
				ceros += ', ';
			}

		}
		fo += '</div>';
		document.getElementById('fo').innerHTML = '<h5><b>Función objetivo</b></h5>' + fo;
		for (i = 1; i <= filas; i++) {

			for (j = 1; j <= columnas; j++) {
				aux += '<input type="text" value="0" name="' + i + 'X' + j + '" id="' + i + 'X' + j + '" onClick="this.select();" required="required" style="text-align:right; width: 8%;" /> <b>X' + j + '</b>';
				if (j != columnas) {
					aux += ' + ';
				}
			}

			matriz += aux + '<input class="boton_personalizado btn-floating pulse btn-small waves-effect waves-light red darken-1" id="btnSig' + i + '" type="button" value="≤" onclick="sig(' + i + ')" /> <input type="text" value="0" name="' + 'R' + i + '" id="' + 'R' + i + '" onClick="this.select();" required="required" style="text-align: right; width : 8%;"  /><br /><br />';
			aux = '';
		}

		document.getElementById('matriz').innerHTML = '<hr><h5><b>Restricciones</b></h5>' + matriz;
		ceros += ' ≥ 0';
		document.getElementById('ceros').innerHTML = '<p class="centered"><b>' + ceros + '</b></p>' + '<div class="row"><a class="waves-effect waves-light btn-large red darken-1"  id="btIterar" onClick="EscojeCamino()" />Resolver</a><hr/></div>';

		document.getElementById('nd').readOnly = true;
		document.getElementById('nr').readOnly = true;
		$("#objetivo").prop("disabled", true);
		$("#nd").prop("disabled", true);
		$("#nr").prop("disabled", true);
		$('select').formSelect();
	}
}

function sig(indice) {
	console.log("Btn ", indice);
	var array = new Array("≤", "=", "≥");
	var x = indice
	for (i = 0; i < array.length; i++) {
		if (document.getElementById("btnSig" + x).value == array[i]) {
			var indice = (i + 1 == array.length) ? 0 : i + 1;
			document.getElementById("btnSig" + x).value = array[indice];
			break;
		}
	}
}

function genera_matriz() {
	var variables = parseInt(document.getElementById('nd').value);
	var restricciones = parseInt(document.getElementById('nr').value);
	crea_matriz(restricciones + 2, variables + restricciones + 2);

	matriz[0][0] = '<b>Z</b>';
	matriz[restricciones + 1][0] = 1;
	matriz[0][variables + restricciones + 1] = '<b>Sol</b>';

	for (i = 1; i <= variables; i++) {
		matriz[0][i] = '<b>X' + i + '</b>';
	}
	for (i = 1; i <= restricciones; i++) {
		matriz[0][i + variables] = '<b>S' + i + '</b>';
	}

	//Llenamos las restricciones y variables S
	for (i = 1; i <= restricciones; i++) {
		console.log($("#btnSig" + i).val())
		for (j = 1; j <= variables; j++) {
			let constante = document.getElementById(i + 'X' + j).value;
			//if ($("#btnSig" + i).val() == "≥") constante *= -1;
			matriz[i][j] = constante;
		}
		let constanteRes = document.getElementById('R' + i).value; // Valor Despues de la igualdad
		//if ($("#btnSig" + i).val() == "≥") constanteRes *= -1; // Si es mayorIgual

		matriz[i][variables + restricciones + 1] = constanteRes;

		for (j = 1; j <= restricciones; j++) {
			if ($("#btnSig" + i).val() == "≥") matriz[i][i + variables] = 1; // Si es mayorIgual
			else if ($("#btnSig" + i).val() == "=") matriz[i][i + variables] = 0;
			else matriz[i][i + variables] = 1;
		}
	}

	//Llenamos Z igualada a cero

	for (j = 1; j <= variables; j++) {
		if (document.getElementById('objetivo').value == 'max')
			matriz[restricciones + 1][j] = (document.getElementById('X' + j).value) * (-1);
		else
			matriz[restricciones + 1][j] = (document.getElementById('X' + j).value);
	}

	imprime_tabla();
}

function imprime_tabla() {
	var variables = parseInt(document.getElementById('nd').value);
	var restricciones = parseInt(document.getElementById('nr').value);
	var filas = restricciones + 2;
	var columnas = variables + restricciones + 2;
	var tabla = '<center><h4>Tabla ' + parseInt(cont + 1) + '</h4></center><table class="striped highlight centered responsive-table" style="text-align:center; border-color:#CCC;" border="1" cellspacing="0" cellpadding="0">';

	for (i = 0; i < filas; i++) {
		tabla += '<tr>';
		for (j = 0; j < columnas; j++) {
			tabla += '<td>' + matriz[i][j] + '</td>';
		}
		tabla += '</tr>';
	}

	tabla += '</table><hr>';

	document.getElementById('tabla').innerHTML += tabla;
	cont++;
}

function esFin() {
	var objetivo = document.getElementById('objetivo').value;
	var variables = parseInt(document.getElementById('nd').value);
	var restricciones = parseInt(document.getElementById('nr').value);

	if (objetivo == 'max') {
		for (j = 1; j <= variables; j++) {
			if (matriz[restricciones + 1][j] < 0) {
				return false;
			}
		}
		for (j = variables + 1; j <= variables + restricciones; j++) {
			if (matriz[restricciones + 1][j] < 0) {
				checkHolguras = 1;
				return false;
			}
		}

		return true;
	}

	if (objetivo == 'min') {
		for (j = 1; j <= variables; j++) {
			if (matriz[restricciones + 1][j] > 0) {
				return false;
			}
		}
		return true;
	}
}

function encuentraPivoteJ() {
	var objetivo = document.getElementById('objetivo').value;
	var variables = parseInt(document.getElementById('nd').value);
	var restricciones = parseInt(document.getElementById('nr').value);
	var itemFila = matriz[restricciones + 1][1];
	pivoteJ = 1;

	if (objetivo == 'max') {
		if (checkHolguras == 1) {
			for (j = variables + 1; j <= variables + restricciones; j++) {
				if (matriz[restricciones + 1][j] < itemFila && matriz[restricciones + 1][j] != 0) {
					itemFila = matriz[restricciones + 1][j];
					pivoteJ = j;
				}
			}
		} else {
			for (j = 1; j <= variables; j++) {
				if (matriz[restricciones + 1][j] < itemFila && matriz[restricciones + 1][j] != 0) {
					itemFila = matriz[restricciones + 1][j];
					pivoteJ = j;
				}
			}
		}
	}

	if (objetivo == 'min') {
		for (j = 1; j <= variables; j++) {
			if (matriz[restricciones + 1][j] > itemFila && matriz[restricciones + 1][j] != 0) {
				itemFila = matriz[restricciones + 1][j];
				pivoteJ = j;
			}
		}
	}

}

function encuentraPivoteI() {
	var restricciones = parseInt(document.getElementById('nr').value);
	var variables = parseInt(document.getElementById('nd').value);
	var razon = 0;
	var aux = Number.MAX_VALUE;
	pivoteI = 0;

	for (i = 1; i <= restricciones; i++) {

		razon = parseFloat(parseFloat(matriz[i][restricciones + variables + 1] / matriz[i][pivoteJ]));

		if (razon > 0 && razon < aux) {
			aux = razon;
			pivoteI = i;
		}
	}
}

function divideFila(i, n) {
	var variables = parseInt(document.getElementById('nd').value);
	var restricciones = parseInt(document.getElementById('nr').value);
	var ncolumnas = variables + restricciones + 2;

	for (j = 0; j < ncolumnas; j++) {
		matriz[i][j] = parseFloat(matriz[i][j]) / n;
	}
}

function SolucionNoAcotada(matriz, pivoteJ, filas) {
	console.log("Comprueba solución no acotada: ");
	var res = true;
	for (var i = 1; i < filas; i++) {
		if (matriz[i][pivoteJ] > 0)
			return false;
	}
	return res;
}

function iterar() {

	document.getElementById('btIterar').disabled = true;
	$(".boton_personalizado").addClass("disabled");
	genera_matriz();
	var objetivo = document.getElementById('objetivo').value;
	var variables = parseInt(document.getElementById('nd').value);
	var restricciones = parseInt(document.getElementById('nr').value);
	var ncolumnas = variables + restricciones + 2;
	var itemAux = 0;
	var respuesta = '<p><b>La solución óptima es.. </b></p>';
	var TieneSolucion = true;
	while (esFin() == false) {
		encuentraPivoteJ();
		if (SolucionNoAcotada(matriz, pivoteJ, restricciones + 2)) {
			respuesta = "Solución no acotada.";
			TieneSolucion = false;
			break;
		}

		encuentraPivoteI();
		divideFila(pivoteI, matriz[pivoteI][pivoteJ]);

		for (i = 1; i <= (restricciones + 1); i++) {
			itemAux = matriz[i][pivoteJ];
			for (j = 0; j < ncolumnas; j++) {
				if (i != pivoteI) {
					//alert( matriz[i][j]+'-'+'('+matriz[i][pivoteJ] +'*'+ matriz[pivoteI][j]+')');
					matriz[i][j] = matriz[i][j] - (itemAux * matriz[pivoteI][j]);
				}

			}

		}


		if (objetivo == "min") { // Multiplicar por -1
			if (esFin()) {
				for (t = variables + 1; t <= variables + restricciones + 1; t++)
					matriz[restricciones + 1][t] *= -1;
			}
		}
		imprime_tabla();
	}
	if (TieneSolucion) {
		if (TieneMultiplesSol(variables, restricciones)) {
			respuesta = "<p class='multiplesSoluciones'>Hay multiples valores en las variables soluciones ";
			for (j = 1; j <= variables; j++)
				respuesta += "X" + j + " ";
			respuesta += "que cumple las restricciones del problema.<br> Una de ellas es:</p><br>"
		}

		for (j = 1; j <= variables; j++) {
			for (i = 1; i <= restricciones; i++) {
				if (matriz[i][j] == 1) {
					respuesta += matriz[0][j] + ' = ' + parseFloat(matriz[i][variables + restricciones + 1]).toFixed(1) +
						' <br />';
				}
			}
		}

		respuesta += 'Z = ' + matriz[restricciones + 1][variables + restricciones + 1];
	}
	document.getElementById('respuesta').innerHTML += '<b>' + respuesta + '</b>' + '<p>¡Fin del proceso!</p><div class="row"><a class="waves-effect waves-light btn-large red darken-1" onClick="location.reload()" /><i class="material-icons right">sync</i>Nuevo Problema</a><hr />';
}

function TieneMultiplesSol(variables, restricciones) {
	var res = false;
	for (t = variables + 1; t <= variables + restricciones; t++)
		if (matriz[restricciones + 1][t] == 0) {
			res = true;
			break;
		}
	return res;
}