var express = require('express');
var app = express(); 
var server = app.listen(process.env.PORT || 3000);
app.use(express.static('public'));
var YASMIJ = require('yasmij');
var socket = require("socket.io");
var io = socket(server);

console.log("RUNNING...")
io.sockets.on("connection", function(socket){
    socket.on('start', function(data){
        console.log("Un usuario se ha conectado")
		var output = YASMIJ.solve( data );
		console.log(JSON.stringify(output, null, 2));
		socket.emit('response', JSON.stringify(output, null, 2));
    })

})
